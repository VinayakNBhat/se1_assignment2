# Memcached Exercise README

Name : Vinayak Narayan Bhat

Matrikel Nr -4661088

Memcahed performance measurement steps:

******************************************************
Building Server and client images:

Build server image : *sudo docker build -t server-image Container1/*

Build client image: *sudo docker build -t client-image Container2/*

******************************************************
Run server and client images: (Please run commands in the same order as below to retain same ip address always)

Run server :  *sudo docker run -d -p 10022:10022 server-image*

Run client :  *sudo docker run -d -p 10023:10023 client-image*

server ip address: 172.17.0.2

client ip address: 172.17.0.3

******************************************************

SSH password for both server and client : 1234  (root login)

*******************************************************
**Experiment execution from client side:**

1. ssh root@127.0.0.1 -p 10023 ./run.sh (password: 1234)*
2. scp -P 10023 root@127.0.0.1:~/graph*.pdf .
3. evince graph*.pdf

Enable passwordless login by follwing commands:

Ex: Passwordless login from server to client: (This is needed to execute experiment form Server side)

login to server : ssh root@127.0.0.1 -p 10022  (password : 1234) 

Execute following commands:
Generating public/private rsa key pair:

	1.ssh-keygen -t rsa (keep every filed blank and press enter)

	2.ssh-copy-id root@172.17.0.3 -p10023 (Password : 1234)

Same for client to server by changing respective ip and port (not needed)

*******************************************************

**Experiment Execution from server side:**

If you want to execute the Experiment from server side just execute 

1. ssh root@127.0.0.1 -p 10022 ./run.sh

2. scp -P 10022 root@127.0.0.1:~/graph*.pdf .

3. evince graph*.pdf