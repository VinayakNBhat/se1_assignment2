library(lattice)

MemCachedData <- read.csv("output-throughput-latency/stats.csv",header=TRUE)

trellis.device("pdf", file="graph1.pdf", color=T, width=6.5, height=5.0)
smoothingSpline = smooth.spline(MemCachedData$ rate , MemCachedData$response, spar=0.35)
plot(MemCachedData$rate ,MemCachedData$response,xlab="Request Rate(Req/s)",ylab="Throughout(Req/s)" , main="Graph 1",pch=16 , type = 'o')

dev.off() -> null 


trellis.device("pdf", file="graph2.pdf", color=T, width=6.5, height=5.0)
smoothingSpline = smooth.spline(MemCachedData$response , MemCachedData$latency, spar=0.35)
plot(MemCachedData$response ,MemCachedData$latency,xlab="Throughout(Req/s)",ylab="Latency(min)"  , main="Graph 2",pch=16 , type = 'o')

dev.off() -> null 
