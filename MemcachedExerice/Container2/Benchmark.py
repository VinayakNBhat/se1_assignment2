import os
import stat
import re

def prepare_exp(SSHHost, SSHPort, REMOTEROOT, optpt):
    
    f = open("run-experiment.sh", 'w')
    f.write("#!/bin/bash\n")

    if optpt["noRequests"] < 50000:
	f.write("mcperf --server=%s -p11211 -R%d -N%d -c1/5 2>Output.txt\n"% (SSHHost,optpt["noRequests"], optpt["noRequests"]))  
    elif optpt["noRequests"] <= 100000:
	f.write("mcperf --server=%s -p11211 -R%d -N%d -c1/5 2>Output.txt\n"% (SSHHost,optpt["noRequests"], optpt["noRequests"])) 
    else: 
	f.write("mcperf --server=%s -p11211 -R%d -N%d -c1/5 2>Output.txt\n"% (SSHHost,optpt["noRequests"], optpt["noRequests"]))

    f.write("REQPERSEC=$(awk '/Request rate:/{print $3F}' Output.txt)\n")
    f.write("RESPRATE=$(awk '/Response rate:/{print $3F}' Output.txt)\n")
    f.write("LATENCY=$(awk '/Response time \[ms\]: avg /{print $5F}' Output.txt)\n")


    f.write("echo \"response,latency\" > stats.csv\n")
    f.write("echo \"$RESPRATE,$LATENCY\" >> stats.csv\n")
    
    f.write("exit $CODE\n")

    f.close()
    
    os.chmod("run-experiment.sh", stat.S_IRWXU) 
